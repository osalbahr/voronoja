#!/usr/bin/python
# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFilter, ImageOps
import math
import random
import time
import numpy as np
import itertools
import colorsys
import random
Ek = time.time()

np.seterr('raise')

# Thank you, Thaddeus Vincenty
# https://geographiclib.sourceforge.io/geodesic-papers/vincenty75b.pdf

# -125 degrees is westernmost, -66 degrees is easternmost
# 24 is southernmost, 50 is northernmost
# seeds are from the top 54 largest MSA in the USA, excluding the MSAs without a city in the top 100 largests cities in the USA, and choosing for the representative city for each MSA the most densely populated city in the name of the MSA; messing with Texas was allowed

# SeedLats = np.array([40.712778, 34.05, 41.881944, 32.705, 29.762778, 38.9101, 25.775163, 39.952778, 33.755, 33.3, 42.373611, 37.7775, 33.948056, 42.331389, 47.609722, 44.981944, 32.627778, 27.973611, 39.7392, 38.627222, 39.289444, 35.227222, 28.303889, 29.425, 45.52, 38.581667, 40.439722, 36.081944, 30.508611, 39.1, 39.099722, 39.962222, 39.768611, 41.483333, 37.354444, 35.846111, 36.916667, 43.05, 30.336944, 35.468611, 35.766667, 35.1175, 37.533333, 29.997778, 38.256111, 42.904722, 32.221667, 36.75])
# using 2020 data: ([40.712778, 33.768333, 41.881944, 32.705, 29.762778, 38.9101, 39.952778, 25.775163, 33.755, 42.373611, 33.3, 37.7775, 33.948056, 42.331389, 47.609722, 44.981944, 32.627778, 27.973611, 39.7392, 39.289444, 38.627222, 28.303889, 35.227222, 29.425, 45.52, 38.581667, 40.439722, 30.508611, 36.175, 39.1, 39.099722, 39.962222, 39.768611, 41.483333, 37.371111, 35.846111, 36.916667, 41.823611, 30.336944, 43.05, 35.468611, 35.766667, 35.1175, 37.533333, 38.256111, 29.997778, 40.760833, 41.7625])
# SeedLons = np.array([-74.006111, -118.25, -87.627778, -97.122778, -95.383056, -77.0147, -80.208615, -75.163611, -84.39, -111.833333, -71.110556, -122.416389, -117.396111, -83.045833, -122.333056, -93.269167, -117.048056, -82.764167, -104.985, -90.197778, -76.615278, -80.843056, -81.412778, -98.493889, -122.681944, -121.494444, -79.976389, -115.124722, -97.678889, -84.516667, -94.578333, -83.000556, -86.158056, -81.666667, -121.969167, -86.391944, -76.2, -87.95, -81.661389, -97.521389, -78.633333, -89.971111, -77.466667, -90.1775, -85.751389, -78.849444, -110.926389, -119.766667])
# using 2020 data: ([-74.006111, -118.195556, -87.627778, -97.122778, -95.383056, -77.0147, -75.163611, -80.208615, -84.39, -71.110556, -111.833333, -122.416389, -117.396111, -83.045833, -122.333056, -93.269167, -117.048056, -82.764167, -104.985, -76.615278, -90.197778, -81.412778, -80.843056, -98.493889, -122.681944, -121.494444, -79.976389, -97.678889, -115.136389, -84.516667, -94.578333, -83.000556, -86.158056, -81.666667, -122.0375, -86.391944, -76.2, -71.422222, -81.661389, -87.95, -97.521389, -78.633333, -89.971111, -77.466667, -85.751389, -90.1775, -111.891111, -72.674167])

SeedLats = np.array([
    40.712778,
    33.768333,
    41.881944,
    32.705,
    29.762778,
    38.9101,
    39.952778,
    25.775163,
    33.755,
    42.373611,
    33.3,
    37.7775,
    33.948056,
    42.331389,
    47.609722,
    44.981944,
    32.627778,
    27.973611,
    39.7392,
    39.289444,
    38.627222,
    28.303889,
    35.227222,
    29.425,
    45.52,
    38.581667,
    40.439722,
    30.508611,
    36.175,
    39.1,
    39.099722,
    39.962222,
    39.768611,
    41.483333,
    37.371111,
    35.846111,
    36.916667,
    41.823611,
    30.336944,
    43.05,
    35.468611,
    35.766667,
    35.1175,
    37.533333,
    38.256111,
    29.997778,
    40.760833,
    41.7625,
])
SeedLons = np.array([
    -74.006111,
    -118.195556,
    -87.627778,
    -97.122778,
    -95.383056,
    -77.0147,
    -75.163611,
    -80.208615,
    -84.39,
    -71.110556,
    -111.833333,
    -122.416389,
    -117.396111,
    -83.045833,
    -122.333056,
    -93.269167,
    -117.048056,
    -82.764167,
    -104.985,
    -76.615278,
    -90.197778,
    -81.412778,
    -80.843056,
    -98.493889,
    -122.681944,
    -121.494444,
    -79.976389,
    -97.678889,
    -115.136389,
    -84.516667,
    -94.578333,
    -83.000556,
    -86.158056,
    -81.666667,
    -122.0375,
    -86.391944,
    -76.2,
    -71.422222,
    -81.661389,
    -87.95,
    -97.521389,
    -78.633333,
    -89.971111,
    -77.466667,
    -85.751389,
    -90.1775,
    -111.891111,
    -72.674167,
])

SemimajorAxis: float = 6378137.0  # a
flattening: float = 1 / 298.257223563  # f
SemiminorAxis: float = (1 - flattening) * SemimajorAxis  # b


def RealDistance(StartLat: float, StartLon: float, EndLat: float, EndLon: float) -> float:
    L: float = np.radians(EndLon - StartLon)
    Ua: float = np.arctan((1 - flattening) *
                          np.tan(np.radians(StartLat)))  # U1
    # U2                 reduced latitude (latitude on the auxiliary sphere)
    Ub: float = np.arctan((1 - flattening) * np.tan(np.radians(EndLat)))

    cosUa: float = np.cos(Ua)
    cosUb: float = np.cos(Ub)
    sinUa: float = np.sin(Ua)
    sinUb: float = np.sin(Ub)

    Convergence: float = 100
    lamdba: float = L

    cosLamdba: float
    sinLamdba: float
    sinsigma: float
    sigma: float
    cossigma: float
    sinAlpha: float
    cosTwoSigmaMid: float
    Cbar: float
    uSquared: float
    Abar: float
    Bbar: float
    DeltaSigma: float
    s: float
    oldS: float

    while np.any(Convergence > 10):
        cosLamdba = np.cos(lamdba)
        sinLamdba = np.sin(lamdba)
        sinsigma = np.sqrt((cosUb * sinLamdba) ** 2 + (cosUa * sinUb
                                                       - cosUb * sinUa * cosLamdba) ** 2)
        cossigma = sinUa * sinUb + cosUa * cosUb * cosLamdba
        sigma = np.arctan2(sinsigma, cossigma)
        try:
            sinAlpha = cosUa * cosUb * sinLamdba / sinsigma
            cosTwoSigmaMid = cossigma - 2 * sinUa * sinUb / (1
                                                             - sinAlpha ** 2)
            Cbar = flattening * (1 - sinAlpha ** 2) * (4 + flattening
                                                       * (4 - 3 * (1 - sinAlpha ** 2))) / 16
            lamdba = L + (1 - Cbar) * flattening * sinAlpha * (sigma
                                                               + Cbar * sinsigma * (cosTwoSigmaMid + Cbar
                                                                                    * cossigma * (2 * cosTwoSigmaMid ** 2 - 1)))
            if np.any(np.abs(lamdba) > np.pi) and np.any(np.abs(180
                                                                - np.hypot(StartLat - EndLat, StartLon - EndLon))
                                                         < 5):
                print(
                    'Seed points and/or mapping area include (nearly) antipodal points')
                break
            uSquared = (1 - sinAlpha ** 2) * (SemimajorAxis ** 2
                                              - SemiminorAxis ** 2) / SemiminorAxis ** 2
            Abar = 1 + uSquared * (4096 + uSquared * (-768 + uSquared
                                                      * (320 - 175 * uSquared))) / 16384
            Bbar = uSquared * (256 + uSquared * (-128 + uSquared * (74
                                                                    - 47 * uSquared))) / 1024
            DeltaSigma = Bbar * sinsigma * (cosTwoSigmaMid + 0.25
                                            * Bbar * (cossigma * (-1 + 2 * cosTwoSigmaMid ** 2)
                                                      - Bbar * cosTwoSigmaMid *
                                                      (-3 + 4 *
                                                       sinsigma ** 2)
                                                      * (-3 + 4 * cosTwoSigmaMid ** 2) / 6))
            s = SemiminorAxis * Abar * (sigma - DeltaSigma)
        except Exception:
            s = 0
        try:
            Convergence = np.abs(s - oldS)
        except Exception:
            pass
        oldS = s
    return s


def Direct(StartLat: float, StartLon: float, distance: float, bearing: float):
    Ua: float = np.arctan(
        (1 - flattening) * np.tan(np.radians(StartLat)))
    sinUa: float = np.sin(Ua)
    cosUa: float = np.cos(Ua)
    cosBearing: float = np.cos(bearing)
    sinBearing: float = np.sin(bearing)
    sigmaA: float = np.arctan2(np.tan(Ua), cosBearing)
    sinAlpha: float = cosUa * sinBearing
    sinAlphaSquared: float = sinAlpha ** 2
    uSquared: float = (1 - sinAlphaSquared) * (SemimajorAxis ** 2
                                               - SemiminorAxis ** 2) / SemiminorAxis ** 2
    Abar: float = 1 + uSquared * (4096 + uSquared * (-768 + uSquared * (320
                                                                        - 175 * uSquared))) / 16384
    Bbar: float = uSquared * (256 + uSquared * (-128 + uSquared * (74 - 47
                                                                   * uSquared))) / 1024
    sigma: float = distance / (Abar * SemiminorAxis)
    Convergence: float = 1000

    cosTwoSigmaMid: float
    cosTwoSigmaMidSquared: float
    cossigma: float
    sinsigma: float
    DeltaSigma: float
    EndLat: float
    OldEndLat: float

    while np.any(Convergence > 0.001 * Scale):
        cosTwoSigmaMid = np.cos(2 * sigmaA + sigma)
        cosTwoSigmaMidSquared = cosTwoSigmaMid ** 2
        cossigma = np.cos(sigma)
        sinsigma = np.sin(sigma)
        DeltaSigma = Bbar * np.sin(sigma) * (cosTwoSigmaMid + Bbar
                                             * (cossigma * (2 * cosTwoSigmaMidSquared - 1) - Bbar
                                                * cosTwoSigmaMid * (4 * (1 - cossigma ** 2) - 3) * (4
                                                                                                    * cosTwoSigmaMidSquared - 3) / 6) / 4)
        sigma = distance / (Abar * SemiminorAxis) + DeltaSigma
        EndLat = np.degrees(np.arctan2(sinUa * cossigma + cosUa
                                       * sinsigma *
                                       cosBearing, (1 -
                                                    flattening)
                                       * np.sqrt(sinAlphaSquared + (sinUa
                                                                    * sinsigma - cosUa * cossigma * cosBearing)
                                                 ** 2)))
        try:
            Convergence = np.abs(EndLat - OldEndLat)
        except Exception:
            pass
        OldEndLat = EndLat
    lamdba: float = np.arctan2(sinsigma * sinBearing, cosUa * cossigma - sinUa
                               * sinsigma * cosBearing)
    Cbar: float = flattening * (1 - sinAlphaSquared) * (4 + flattening * (4
                                                                          - 3 * (1 - sinAlphaSquared))) / 16
    L: float = lamdba - (1 - Cbar) * flattening * sinAlpha * (sigma + Cbar
                                                              * sinsigma * (cosTwoSigmaMid + Cbar * cossigma * (2
                                                                                                                * cosTwoSigmaMidSquared - 1)))
    EndLon: float = np.degrees(L) + StartLon
    return np.array([EndLat, EndLon])


def SphericalDirect(StartLat: float, StartLon: float, distance: float, bearing: float):
    StartLat = np.radians(StartLat)
    StartLon = np.radians(StartLon)
    radius: float = np.sqrt(((SemimajorAxis ** 2 * np.cos(StartLat)) ** 2
                             + (SemiminorAxis ** 2 * np.sin(StartLat)) ** 2)
                            / ((SemimajorAxis * np.cos(StartLat)) ** 2
                               + (SemiminorAxis * np.sin(StartLat)) ** 2))
    sigma: float = distance / radius
    EndLat: float = np.degrees(np.arcsin(np.sin(
        StartLat) * np.cos(sigma) + np.cos(StartLat)*np.sin(sigma)*np.cos(bearing)))
    EndLon: float = np.degrees(np.arctan2(np.sin(bearing)*np.sin(sigma)*np.cos(
        StartLat), np.cos(sigma) - np.sin(StartLat)*np.sin(np.radians(EndLat))) + StartLon)
    return np.array([EndLat, EndLon])


def SphericalDistance(StartLat: float, StartLon: float, EndLat: float, EndLon: float) -> float:
    L: float = np.radians(EndLon - StartLon)
    deltaLat: float = np.radians(EndLat - StartLat)
    StartLat = np.radians(StartLat)
    EndLat = np.radians(EndLat)
    # a better approximation of the average latitude would be this if deltaLat>L, and max(StartLat,EndLat) otherwise
    Lat: float = 0.5 * (StartLat + EndLat)
    radius: float = np.sqrt(((SemimajorAxis ** 2 * np.cos(Lat)) ** 2
                             + (SemiminorAxis ** 2 * np.sin(Lat)) ** 2)
                            / ((SemimajorAxis * np.cos(Lat)) ** 2
                               + (SemiminorAxis * np.sin(Lat)) ** 2))
    # midpoint Lat should be np.arctan2(np.sin(StartLat) + np.sin(EndLat), np.sqrt((cos(StartLat) + cos(EndLat)*cos(L))**2 + (cos(EndLat)*sin(L))**2)); but this is not the average latitude
    if np.all(np.absolute(EndLon - StartLon) < 150):
        optimized: float = 2 * radius \
            * np.arcsin(np.sqrt(np.square(np.sin(deltaLat / 2))
                        + np.cos(StartLat) * np.cos(EndLat)
                        * np.square(np.sin(L / 2))))
    else:
        optimized: float = np.arctan(np.sqrt(np.square(np.cos(EndLat)
                                                       * np.sin(L)) + np.square(np.cos(StartLat)
                                                                                * np.sin(EndLat) - np.sin(StartLat)
                                                                                * np.cos(EndLat) * np.cos(L)))
                                     / (np.sin(StartLat) * np.sin(EndLat)
                                        + np.cos(StartLat) *
                                        np.cos(EndLat)
                                        * np.cos(L)))
    return optimized


def OptimizedDistance(StartLat: float, StartLon: float, EndLat: float, EndLon: float) -> float:
    if np.amin(np.absolute(EndLon - StartLon)) <= Scale \
            and np.nanmin(np.absolute(np.hypot(EndLat - StartLat, EndLon
                          - StartLon) * np.cbrt(EndLat - StartLat))) \
            <= Scale / np.e:
        optimized: float = SphericalDistance(StartLat, StartLon, EndLat,
                                             EndLon)
    else:
        optimized = RealDistance(
            StartLat, StartLon, EndLat, EndLon)
    return optimized


im = Image.open('~/Desktop/USA population density2.png')
im = im.convert('L')
# im = np.asarray(Image.open('~/Downloads/GHS_POP_E2015_GLOBE_R2019A_4326_9ss_V1_0/GHS_POP_E2015_GLOBE_R2019A_4326_9ss_V1_0.tif'))
original = Image.open(
    '~/Desktop/VoronoiDiagramNoWeight.png')
paths = original.copy()
edge = original.copy()

Scale = 1
num_cells: int = len(SeedLats)
northmost: float = 5000 / Scale
eastmost: float = -6600 / Scale
southmost: float = 2400 / Scale
westmost: float = -12500 / Scale
width: int = int(eastmost - westmost)
height: int = int(northmost - southmost)
image = original.copy()
putpixel = image.putpixel
draw = ImageDraw.Draw(image)
(imgx, imgy) = image.size

edge = ImageOps.expand(edge, border=2, fill=(220, 220, 220))
edge = edge.filter(ImageFilter.Kernel((3, 3), (-1, -1, -1,
                                               -1, 8, -1,
                                               -1, -1, -1),
                                      1, 0)).convert('L').point(lambda x: (255 if x > 0 else 0),
                                                                mode='1').convert('L'
                                                                                  ).filter(ImageFilter.BoxBlur(1)).point(lambda x: (255 if x
                                                                                                                                    > 80 else 0), mode='1').convert('L').crop((2, 2, width + 2,
                                                                                                                                                                               height + 2))


# def PopulationDensity(StartLat: float, StartLon: float, EndLat: float, EndLon: float) -> float:
#     x: int = int(100 * StartLon / Scale - westmost)
#     y: int = int(northmost - 100 * StartLat / Scale)
#     startx: int = x
#     starty: int = y
#     dn: int = 1
#
#     Luminosity: float = 0
#     oldLuminosity: float = 0
#     PopDensity: float = 0
#     endx: int = int(100 * EndLon / Scale - westmost)
#     endy: int = int(northmost - 100 * EndLat / Scale)
#     slope: float
#     water: int = 0
#
#     if endy - y > 0:
#         if endx - x > 0:  # bottom left to top right
#             if abs(endx - x) > abs(endy - y):
#                 slope = (endy - y) / (endx - x)
#                 for t in range(endx - x):
#                     x = startx + t
#                     y = starty + math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#             else:
#                 slope = (endx - x) / (endy - y)
#                 for t in range(endy - y):
#                     y = starty + t
#                     x = startx + math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#         elif endx - x == 0:  # bottom to top
#             for t in range(endy - y):
#                 y = starty + t
#                 try:
#                     Luminosity = (im.getpixel(
#                         (x, y)) + (dn - 1) * oldLuminosity) / dn
#                     if im.getpixel((x, y)) > 238:
#                         water += 1
#                 except IndexError:
#                     print("error at {},{}".format(x, y))
#                     Luminosity = (dn - 1) * \
#                         oldLuminosity / dn
#                 dn += 1
#                 oldLuminosity = Luminosity
#         else:  # bottom right to top left
#             if abs(endx - x) > abs(endy - y):
#                 slope = abs((endy - y) / (endx - x))
#                 for t in range(endx - x):
#                     x = startx - t
#                     y = starty + math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#             else:
#                 slope = abs((endy - y) / (endx - x))
#                 for t in range(endx - x):
#                     y = starty + t
#                     x = startx - math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#     elif endy - y == 0:
#         if endx - x > 0:  # left to right
#             for t in range(endx - x):
#                 x = startx + t
#                 try:
#                     Luminosity = (im.getpixel(
#                         (x, y)) + (dn - 1) * oldLuminosity) / dn
#                     if im.getpixel((x, y)) > 238:
#                         water += 1
#                 except IndexError:
#                     print("error at {},{}".format(x, y))
#                     Luminosity = (dn - 1) * \
#                         oldLuminosity / dn
#                 dn += 1
#                 oldLuminosity = Luminosity
#         else:  # right to left
#             for t in range(endx - x):
#                 x = startx - t
#                 try:
#                     Luminosity = (im.getpixel(
#                         (x, y)) + (dn - 1) * oldLuminosity) / dn
#                     if im.getpixel((x, y)) > 238:
#                         water += 1
#                 except IndexError:
#                     print("error at {},{}".format(x, y))
#                     Luminosity = (dn - 1) * \
#                         oldLuminosity / dn
#                 dn += 1
#                 oldLuminosity = Luminosity
#     else:
#         if endx - x > 0:  # top left to bottom right
#             if abs(endx - x) > abs(endy - y):
#                 slope = abs((endy - y) / (endx - x))
#                 for t in range(endx - x):
#                     x = startx + t
#                     y = starty - math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#             else:
#                 slope = abs((endx - x) / (endy - y))
#                 for t in range(endy - y):
#                     y = starty - t
#                     x = startx + math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#         elif endx - x == 0:  # top to bottom
#             for t in range(endy - y):
#                 y = starty - t
#                 try:
#                     Luminosity = (im.getpixel(
#                         (x, y)) + (dn - 1) * oldLuminosity) / dn
#                     if im.getpixel((x, y)) > 238:
#                         water += 1
#                 except IndexError:
#                     print("error at {},{}".format(x, y))
#                     Luminosity = (dn - 1) * \
#                         oldLuminosity / dn
#                 dn += 1
#                 oldLuminosity = Luminosity
#         else:  # top right to bottom left
#             if abs(endx - x) > abs(endy - y):
#                 slope = abs((endy - y) / (endx - x))
#                 for t in range(endx - x):
#                     x = startx - t
#                     y = starty - math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#             else:
#                 slope = abs((endy - y) / (endx - x))
#                 for t in range(endx - x):
#                     y = starty - t
#                     x = startx - math.floor(t * slope)
#                     try:
#                         Luminosity = (im.getpixel(
#                             (x, y)) + (dn - 1) * oldLuminosity) / dn
#                         if im.getpixel((x, y)) > 238:
#                             water += 1
#                     except IndexError:
#                         print("error at {},{}".format(x, y))
#                         Luminosity = (dn - 1) * oldLuminosity / dn
#                     dn += 1
#                     oldLuminosity = Luminosity
#     Luminosity = Luminosity / \
#         SphericalDistance(StartLat, StartLon, EndLat, EndLon)
#     if Luminosity >= 224:
#         PopDensity = 12.2 - Luminosity * 0.05
#     elif 224 > Luminosity >= 215:
#         PopDensity = 50.7777777777778 - Luminosity * 0.222222222222222
#     elif 215 > Luminosity >= 205:
#         PopDensity = 46 - Luminosity / 5
#     elif 205 > Luminosity >= 196:
#         PopDensity = 118.888888888889 - Luminosity * 0.555555555555556
#     elif 196 > Luminosity >= 186:
#         PopDensity = 304 - Luminosity * 1.5
#     elif 186 > Luminosity >= 178:
#         PopDensity = 606.25 - Luminosity * 3.125
#     elif 178 > Luminosity >= 168:
#         PopDensity = 940 - Luminosity * 5
#     elif 168 > Luminosity >= 152:
#         PopDensity = 1150 - Luminosity * 6.25
#     elif 152 > Luminosity >= 145:
#         PopDensity = 2371.42857142857 - Luminosity * 14.2857142857143
#     elif 145 > Luminosity >= 136:
#         PopDensity = 1911.11111111111 - Luminosity * 11.1111111111111
#     elif 136 > Luminosity >= 130:
#         PopDensity = 2666.66666666667 - Luminosity * 16.6666666666667
#     elif 130 > Luminosity >= 122:
#         PopDensity = 2125 - Luminosity * 12.5
#     elif 122 > Luminosity >= 114:
#         PopDensity = 3650 - Luminosity * 25
#     elif 114 > Luminosity >= 107:
#         PopDensity = 4057.14285714286 - Luminosity * 28.5714285714286
#     elif 107 > Luminosity >= 100:
#         PopDensity = 4821.42857142857 - Luminosity * 35.7142857142857
#     elif 100 > Luminosity >= 94:
#         PopDensity = 5416.66666666667 - Luminosity * 41.6666666666667
#     elif 94 > Luminosity >= 87:
#         PopDensity = 4857.14285714286 - Luminosity * 35.7142857142857
#     elif 87 > Luminosity >= 81:
#         PopDensity = 5375 - Luminosity * 41.6666666666667
#     elif 81 > Luminosity >= 74:
#         PopDensity = 7785.71428571429 - Luminosity * 71.4285714285714
#     elif 74 > Luminosity >= 68:
#         PopDensity = 8666.66666666667 - Luminosity * 83.3333333333333
#     elif 68 > Luminosity >= 62:
#         PopDensity = 14333.3333333333 - Luminosity * 166.666666666667
#     elif 62 > Luminosity >= 48:
#         PopDensity = 21714.2857142857 - Luminosity * 285.714285714286
#     elif 48 > Luminosity >= 40:
#         PopDensity = 20000 - Luminosity * 250
#     elif 40 > Luminosity >= 28:
#         PopDensity = 60000 - Luminosity * 1250
#     else:
#         PopDensity = 78846.1538461538 - Luminosity * 1923.07692307692
#     if PopDensity < 0:
#         PopDensity = 100000
#     if water > 0.05 * np.linalg.norm((endy-starty, endx-startx)):
#         PopDensity = 100000
#     print(PopDensity)
#     return PopDensity


def PopulationDensity(StartLat: float, StartLon: float, EndLat: float, EndLon: float) -> float:  # good, unsimplified
    L: float = np.radians(EndLon - StartLon)
    Ua: float = np.arctan(
        (1 - flattening) * np.tan(np.radians(StartLat)))
    Ub: float = np.arctan((1 - flattening) * np.tan(np.radians(EndLat)))
    cosUa: float = np.cos(Ua)
    cosUb: float = np.cos(Ub)
    sinUa: float = np.sin(Ua)
    sinUb: float = np.sin(Ub)
    Convergence: float = 100
    lamdba: float = L

    cosLamdba: float
    sinLamdba: float
    sinsigma: float
    cossigma: float
    sigma: float
    sinAlpha: float
    cosTwoSigmaMid: float
    Cbar: float
    uSquared: float
    Abar: float
    Bbar: float
    DeltaSigma: float
    distance: float
    oldDistance: float

    while Convergence > 10:
        cosLamdba = np.cos(lamdba)
        sinLamdba = np.sin(lamdba)
        sinsigma = np.sqrt((cosUb * sinLamdba) ** 2 + (cosUa * sinUb
                                                       - cosUb * sinUa * cosLamdba) ** 2)
        cossigma = sinUa * sinUb + cosUa * cosUb * cosLamdba
        sigma = np.arctan2(sinsigma, cossigma)
        try:
            sinAlpha = cosUa * cosUb * sinLamdba / sinsigma
            cosTwoSigmaMid = cossigma - 2 * sinUa * sinUb / (1
                                                             - sinAlpha ** 2)
            Cbar = flattening * (1 - sinAlpha ** 2) * (4 + flattening
                                                       * (4 - 3 * (1 - sinAlpha ** 2))) / 16
            lamdba = L + (1 - Cbar) * flattening * sinAlpha * (sigma
                                                               + Cbar * sinsigma * (cosTwoSigmaMid + Cbar
                                                                                    * cossigma * (2 * cosTwoSigmaMid ** 2 - 1)))
            if np.abs(lamdba) > np.pi and np.abs(180
                                                 - np.hypot(StartLat - EndLat, StartLon - EndLon)) \
                    < 5:
                print(
                    'Seed points and/or mapping area include (nearly) antipodal points')
                break
            uSquared = (1 - sinAlpha ** 2) * (SemimajorAxis ** 2
                                              - SemiminorAxis ** 2) / SemiminorAxis ** 2
            Abar = 1 + uSquared * (4096 + uSquared * (-768 + uSquared
                                                      * (320 - 175 * uSquared))) / 16384
            Bbar = uSquared * (256 + uSquared * (-128 + uSquared * (74
                                                                    - 47 * uSquared))) / 1024
            DeltaSigma = Bbar * sinsigma * (cosTwoSigmaMid + 0.25
                                            * Bbar * (cossigma * (-1 + 2 * cosTwoSigmaMid ** 2)
                                                      - Bbar * cosTwoSigmaMid *
                                                      (-3 + 4 *
                                                      sinsigma ** 2)
                                                      * (-3 + 4 * cosTwoSigmaMid ** 2) / 6))
            distance = SemiminorAxis * \
                Abar * (sigma - DeltaSigma)
        except Exception:
            distance = 0
        try:
            Convergence = np.abs(distance - oldDistance)
        except Exception:
            pass
        oldDistance = distance
    bearing: float = math.atan2(cosUb * sinLamdba, cosUa * sinUb - sinUa
                                * cosUb * cosLamdba)

    dn: int = 1
    x: int = int(100 * StartLon / Scale - westmost)
    y: int = int(northmost - 100 * StartLat / Scale)

    startx = x
    starty = y

    # line segments
    Luminosity: float = 0
    oldLuminosity: float = 0
    PopDensity: float = 0
    endx: int
    endy: int
    slope: float
    water: int = 0

    if distance > 50000:

        for n in range(1, math.floor(distance / 50000)):
            if water > 99:
                break
            Next = Direct(StartLat, StartLon,
                          n * 50000, bearing)  # modify to use the fractional point between formula into a list
            NextLat = Next[0]
            NextLon = Next[1]
            endy = int(northmost - 100 * NextLat / Scale)
            endx = int(100 * NextLon / Scale - westmost)
            if endy - y > 0:
                if endx - x > 0:  # bottom left to top right
                    if abs(endx - x) > abs(endy - y):
                        slope = (endy - y) / (endx - x)
                        for t in range(endx - x):
                            x = startx + t
                            y = starty + math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
                    else:
                        slope = (endx - x) / (endy - y)
                        for t in range(endy - y):
                            y = starty + t
                            x = startx + math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
                elif endx - x == 0:  # bottom to top
                    for t in range(endy - y):
                        y = starty + t
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * \
                                oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                else:  # bottom right to top left
                    if abs(endx - x) > abs(endy - y):
                        slope = abs((endy - y) / (endx - x))
                        for t in range(endx - x):
                            x = startx - t
                            y = starty + math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
                    else:
                        slope = abs((endy - y) / (endx - x))
                        for t in range(endx - x):
                            y = starty + t
                            x = startx - math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
            elif endy - y == 0:
                if endx - x > 0:  # left to right
                    for t in range(endx - x):
                        x = startx + t
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * \
                                oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                if endx - x < 0:  # right to left
                    for t in range(endx - x):
                        x = startx - t
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * \
                                oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
            else:
                if endx - x > 0:  # top left to bottom right
                    if abs(endx - x) > abs(endy - y):
                        slope = abs((endy - y) / (endx - x))
                        for t in range(endx - x):
                            x = startx + t
                            y = starty - math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
                    else:
                        slope = abs((endx - x) / (endy - y))
                        for t in range(endy - y):
                            y = starty - t
                            x = startx + math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
                elif endx - x == 0:  # top to bottom
                    for t in range(endy - y):
                        y = starty - t
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * \
                                oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                else:  # top right to bottom left
                    if abs(endx - x) > abs(endy - y):
                        slope = abs((endy - y) / (endx - x))
                        for t in range(endx - x):
                            x = startx - t
                            y = starty - math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
                    else:
                        slope = abs((endy - y) / (endx - x))
                        for t in range(endx - x):
                            y = starty - t
                            x = startx - math.floor(t * slope)
                            try:
                                Luminosity = (im.getpixel(
                                    (x, y)) + (dn - 1) * oldLuminosity) / dn
                                if im.getpixel((x, y)) > 238:
                                    water += 1
                            except IndexError:
                                # print("error at {},{}".format(x, y))
                                Luminosity = (dn - 1) * \
                                    oldLuminosity / dn
                            dn += 1
                            oldLuminosity = Luminosity
    else:
        # under 50km
        endy = int(northmost - 100 * EndLat / Scale)
        endx = int(100 * EndLon / Scale - westmost)
        if endy - y > 0:
            if endx - x > 0:  # bottom left to top right
                if abs(endx - x) > abs(endy - y):
                    slope = (endy - y) / (endx - x)
                    for t in range(endx - x):
                        x = startx + t
                        y = starty + math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                else:
                    slope = (endx - x) / (endy - y)
                    for t in range(endy - y):
                        y = starty + t
                        x = startx + math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
            elif endx - x == 0:  # bottom to top
                for t in range(endy - y):
                    y = starty + t
                    try:
                        Luminosity = (im.getpixel(
                            (x, y)) + (dn - 1) * oldLuminosity) / dn
                        if im.getpixel((x, y)) > 238:
                            water += 1
                    except IndexError:
                        # print("error at {},{}".format(x, y))
                        Luminosity = (dn - 1) * \
                            oldLuminosity / dn
                    dn += 1
                    oldLuminosity = Luminosity
            else:  # bottom right to top left
                if abs(endx - x) > abs(endy - y):
                    slope = abs((endy - y) / (endx - x))
                    for t in range(endx - x):
                        x = startx - t
                        y = starty + math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                else:
                    slope = abs((endy - y) / (endx - x))
                    for t in range(endx - x):
                        y = starty + t
                        x = startx - math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
        elif endy - y == 0:
            if endx - x > 0:  # left to right
                for t in range(endx - x):
                    x = startx + t
                    try:
                        Luminosity = (im.getpixel(
                            (x, y)) + (dn - 1) * oldLuminosity) / dn
                        if im.getpixel((x, y)) > 238:
                            water += 1
                    except IndexError:
                        # print("error at {},{}".format(x, y))
                        Luminosity = (dn - 1) * \
                            oldLuminosity / dn
                    dn += 1
                    oldLuminosity = Luminosity
            else:  # right to left
                for t in range(endx - x):
                    x = startx - t
                    try:
                        Luminosity = (im.getpixel(
                            (x, y)) + (dn - 1) * oldLuminosity) / dn
                        if im.getpixel((x, y)) > 238:
                            water += 1
                    except IndexError:
                        # print("error at {},{}".format(x, y))
                        Luminosity = (dn - 1) * \
                            oldLuminosity / dn
                    dn += 1
                    oldLuminosity = Luminosity
        else:
            if endx - x > 0:  # top left to bottom right
                if abs(endx - x) > abs(endy - y):
                    slope = abs((endy - y) / (endx - x))
                    for t in range(endx - x):
                        x = startx + t
                        y = starty - math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                else:
                    slope = abs((endx - x) / (endy - y))
                    for t in range(endy - y):
                        y = starty - t
                        x = startx + math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
            elif endx - x == 0:  # top to bottom
                for t in range(endy - y):
                    y = starty - t
                    try:
                        Luminosity = (im.getpixel(
                            (x, y)) + (dn - 1) * oldLuminosity) / dn
                        if im.getpixel((x, y)) > 238:
                            water += 1
                    except IndexError:
                        # print("error at {},{}".format(x, y))
                        Luminosity = (dn - 1) * \
                            oldLuminosity / dn
                    dn += 1
                    oldLuminosity = Luminosity
            else:  # top right to bottom left
                if abs(endx - x) > abs(endy - y):
                    slope = abs((endy - y) / (endx - x))
                    for t in range(endx - x):
                        x = startx - t
                        y = starty - math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
                else:
                    slope = abs((endy - y) / (endx - x))
                    for t in range(endx - x):
                        y = starty - t
                        x = startx - math.floor(t * slope)
                        try:
                            Luminosity = (im.getpixel(
                                (x, y)) + (dn - 1) * oldLuminosity) / dn
                            if im.getpixel((x, y)) > 238:
                                water += 1
                        except IndexError:
                            # print("error at {},{}".format(x, y))
                            Luminosity = (dn - 1) * oldLuminosity / dn
                        dn += 1
                        oldLuminosity = Luminosity
    if Luminosity >= 224:
        PopDensity = 12.2 - Luminosity * 0.05
    elif 224 > Luminosity >= 215:
        PopDensity = 50.7777777777778 - Luminosity * 0.222222222222222
    elif 215 > Luminosity >= 205:
        PopDensity = 46 - Luminosity / 5
    elif 205 > Luminosity >= 196:
        PopDensity = 118.888888888889 - Luminosity * 0.555555555555556
    elif 196 > Luminosity >= 186:
        PopDensity = 304 - Luminosity * 1.5
    elif 186 > Luminosity >= 178:
        PopDensity = 606.25 - Luminosity * 3.125
    elif 178 > Luminosity >= 168:
        PopDensity = 940 - Luminosity * 5
    elif 168 > Luminosity >= 152:
        PopDensity = 1150 - Luminosity * 6.25
    elif 152 > Luminosity >= 145:
        PopDensity = 2371.42857142857 - Luminosity * 14.2857142857143
    elif 145 > Luminosity >= 136:
        PopDensity = 1911.11111111111 - Luminosity * 11.1111111111111
    elif 136 > Luminosity >= 130:
        PopDensity = 2666.66666666667 - Luminosity * 16.6666666666667
    elif 130 > Luminosity >= 122:
        PopDensity = 2125 - Luminosity * 12.5
    elif 122 > Luminosity >= 114:
        PopDensity = 3650 - Luminosity * 25
    elif 114 > Luminosity >= 107:
        PopDensity = 4057.14285714286 - Luminosity * 28.5714285714286
    elif 107 > Luminosity >= 100:
        PopDensity = 4821.42857142857 - Luminosity * 35.7142857142857
    elif 100 > Luminosity >= 94:
        PopDensity = 5416.66666666667 - Luminosity * 41.6666666666667
    elif 94 > Luminosity >= 87:
        PopDensity = 4857.14285714286 - Luminosity * 35.7142857142857
    elif 87 > Luminosity >= 81:
        PopDensity = 5375 - Luminosity * 41.6666666666667
    elif 81 > Luminosity >= 74:
        PopDensity = 7785.71428571429 - Luminosity * 71.4285714285714
    elif 74 > Luminosity >= 68:
        PopDensity = 8666.66666666667 - Luminosity * 83.3333333333333
    elif 68 > Luminosity >= 62:
        PopDensity = 14333.3333333333 - Luminosity * 166.666666666667
    elif 62 > Luminosity >= 48:
        PopDensity = 21714.2857142857 - Luminosity * 285.714285714286
    elif 48 > Luminosity >= 40:
        PopDensity = 20000 - Luminosity * 250
    elif 40 > Luminosity >= 28:
        PopDensity = 60000 - Luminosity * 1250
    else:
        PopDensity = 78846.1538461538 - Luminosity * 1923.07692307692
    if PopDensity < 0:
        PopDensity = 100000
    PopDensity = Luminosity / \
        SphericalDistance(StartLat, StartLon, EndLat, EndLon)
    if water > 0.05 * np.linalg.norm((int(northmost - 100 * EndLat / Scale)-int(northmost - 100 * StartLat / Scale), int(100 * EndLon / Scale - westmost)-int(100 * StartLon / Scale - westmost))):
        PopDensity = 100000
    print(PopDensity)
    return PopDensity


nh = 100 * (SeedLats - Scale * southmost / 100) / height
nv = 0.4 + 40 * (SeedLons - Scale * westmost / 100) / width
nr = np.empty(num_cells)
ng = np.empty(num_cells)
nb = np.empty(num_cells)
for n in range(num_cells):
    nrgb = colorsys.hsv_to_rgb(nh[n], 1, nv[n])
    nr[n] = nrgb[0]
    ng[n] = nrgb[1]
    nb[n] = nrgb[2]
nr = np.ceil(nr * 256).astype(int)
ng = np.ceil(ng * 256).astype(int)
nb = np.ceil(nb * 256).astype(int)

SeedX = 100 * SeedLons / Scale - westmost
SeedY = northmost - 100 * SeedLats / Scale

weighted: float
x: int
startx: int
y: int
starty: int
popd: float
for (x, y) in itertools.product(range(imgx), range(imgy)):
    if edge.getpixel((x, y)) == 255 and x not in SeedX and y \
            not in SeedY and random.randrange(0, 100) == 42:

        minweighted: float = 1000000000000
        j: int
        minj: int
        for j in range(num_cells):
            weighted = PopulationDensity(
                Scale * (northmost - y) / 100, Scale * (westmost + x) / 100, SeedLats[j], SeedLons[j]) * OptimizedDistance(
                Scale * (northmost - y) / 100, Scale * (westmost + x) / 100, SeedLats[j], SeedLons[j])
            if weighted < minweighted:
                minweighted = weighted
                minj = j
        if image.getpixel((x, y)) != (nr[minj], ng[minj], nb[minj]):
            draw.line(((x, y), (100 * SeedLons[minj] / Scale - westmost, northmost - 100 *
                      SeedLats[minj] / Scale)), fill=((0, 0, 0)), width=1)  # (nr[minj], ng[minj], nb[minj])), width=1)
            # draw.rectangle([(x - 24, y + 24), (x + 25, y - 25)], fill=((nr[minj], ng[minj], nb[minj])), outline=((nr[minj], ng[minj], nb[minj])))
            # distance = OptimizedDistance(Scale * (northmost - y) / 100, Scale * (
            #     westmost + x) / 100, SeedLats[minj], SeedLons[minj])
            # if distance <= 50000:
            #     endy = northmost - 100 * SeedLats[minj] / Scale
            #     endx = 100 * SeedLons[minj] / Scale - westmost
            #     draw.line(((x, y), (endx, endy)), fill=(
            #         (nr[minj], ng[minj], nb[minj])), width=1)
            # else:
            #     StartLat: float = Scale * (northmost - y) / 100
            #     StartLon: float = Scale * (westmost + x) / 100
            #     L: float = np.radians(SeedLons[minj] - StartLon)
            #     # Ua: float = np.arctan(
            #     #     (1 - flattening) * np.tan(np.radians(StartLat)))
            #     # Ub: float = np.arctan(
            #     #     (1 - flattening) * np.tan(np.radians(SeedLats[minj])))
            #     # cosUa: float = np.cos(Ua)
            #     # cosUb: float = np.cos(Ub)
            #     # sinUa: float = np.sin(Ua)
            #     # sinUb: float = np.sin(Ub)
            #     # Convergence: float = 100
            #     # lamdba: float = L
            #     #
            #     # cosLamdba: float
            #     # sinLamdba: float
            #     # sinsigma: float
            #     # cossigma: float
            #     # sigma: float
            #     # sinAlpha: float
            #     # cosTwoSigmaMid: float
            #     # Cbar: float
            #     # uSquared: float
            #     # Abar: float
            #     # Bbar: float
            #     # DeltaSigma: float
            #     # distance: float
            #     # oldDistance: float
            #     #
            #     # while Convergence > 10:
            #     #     cosLamdba = np.cos(lamdba)
            #     #     sinLamdba = np.sin(lamdba)
            #     #     sinsigma = np.sqrt((cosUb * sinLamdba) ** 2 + (cosUa * sinUb
            #     #                                                    - cosUb * sinUa * cosLamdba) ** 2)
            #     #     cossigma = sinUa * sinUb + cosUa * cosUb * cosLamdba
            #     #     sigma = np.arctan2(sinsigma, cossigma)
            #     #     try:
            #     #         sinAlpha = cosUa * cosUb * sinLamdba / sinsigma
            #     #         cosTwoSigmaMid = cossigma - 2 * sinUa * sinUb / (1
            #     #                                                          - sinAlpha ** 2)
            #     #         Cbar = flattening * (1 - sinAlpha ** 2) * (4 + flattening
            #     #                                                    * (4 - 3 * (1 - sinAlpha ** 2))) / 16
            #     #         lamdba = L + (1 - Cbar) * flattening * sinAlpha * (sigma
            #     #                                                            + Cbar * sinsigma * (cosTwoSigmaMid + Cbar
            #     #                                                                                 * cossigma * (2 * cosTwoSigmaMid ** 2 - 1)))
            #     #         if np.abs(lamdba) > np.pi and np.abs(180
            #     #                                              - np.hypot(StartLat - EndLat, StartLon - EndLon)) \
            #     #                 < 5:
            #     #             print(
            #     #                 'Seed points and/or mapping area include (nearly) antipodal points')
            #     #             break
            #     #         uSquared = (1 - sinAlpha ** 2) * (SemimajorAxis ** 2
            #     #                                           - SemiminorAxis ** 2) / SemiminorAxis ** 2
            #     #         Abar = 1 + uSquared * (4096 + uSquared * (-768 + uSquared
            #     #                                                   * (320 - 175 * uSquared))) / 16384
            #     #         Bbar = uSquared * (256 + uSquared * (-128 + uSquared * (74
            #     #                                                                 - 47 * uSquared))) / 1024
            #     #         DeltaSigma = Bbar * sinsigma * (cosTwoSigmaMid + 0.25
            #     #                                         * Bbar * (cossigma * (-1 + 2 * cosTwoSigmaMid ** 2)
            #     #                                                   - Bbar * cosTwoSigmaMid *
            #     #                                                   (-3 + 4 *
            #     #                                                    sinsigma ** 2)
            #     #                                                   * (-3 + 4 * cosTwoSigmaMid ** 2) / 6))
            #     #         distance = SemiminorAxis * \
            #     #             Abar * (sigma - DeltaSigma)
            #     #     except Exception:
            #     #         distance = 0
            #     #     try:
            #     #         Convergence = np.abs(distance - oldDistance)
            #     #     except Exception:
            #     #         pass
            #     #     oldDistance = distance
            #     # bearing: float = math.atan2(cosUb * sinLamdba, cosUa * sinUb - sinUa
            #     #                             * cosUb * cosLamdba)
            #     bearing: float = math.atan2(np.cos(np.radians(SeedLats[minj])) * np.sin(L), np.cos(np.radians(StartLat)) * np.sin(np.radians(
            #         SeedLats[minj])) - np.sin(np.radians(StartLat)) * np.cos(np.radians(SeedLats[minj]))*np.cos(L))
            #     n: int
            #     for n in range(1, math.floor(distance / 50000) - 1):
            #         Next = SphericalDirect(
            #             StartLat, StartLon, n * 50000, bearing)
            #         NextLat = Next[0]
            #         NextLon = Next[1]
            #         endy: int = int(northmost - 100 * NextLat / Scale)
            #         endx: int = int(100 * NextLon / Scale - westmost)
            #         try:
            #             draw.line(((startx, starty), (endx, endy)), fill=(
            #                 (nr[minj], ng[minj], nb[minj])), width=1)
            #         except NameError:
            #             draw.line(((x, y), (endx, endy)), fill=(
            #                 (nr[minj], ng[minj], nb[minj])), width=1)
            #         startx = endx
            #         starty = endy
            #     draw.line(((x, y), (SeedX[minj], SeedY[minj])), fill=(
            #         (nr[minj], ng[minj], nb[minj])), width=1)

            # maybe do the edge recalculate and put line step twice, with the first iteration being two thick if the line method is better than squares

# n: int
# x = 356
# y = 1008
# # (Scale * (northmost - y) / 100, Scale * (westmost + x) / 100,
# bearing = math.atan2(np.cos(np.radians(SeedLats[7])) * np.sin(np.radians(SeedLons[7]-Scale * (westmost + x) / 100)), np.cos(np.radians(Scale * (northmost - y) / 100)) * np.sin(np.radians(
#     SeedLats[7])) - np.sin(np.radians(Scale * (northmost - y) / 100)) * np.cos(np.radians(SeedLats[7]))*np.cos(np.radians(SeedLons[7]-Scale * (westmost + x) / 100)))
# for n in range(1, 50):
#     Next = SphericalDirect(Scale * (northmost - y) / 100,
#                            Scale * (westmost + x) / 100, n * 50000, bearing)
#     NextLat = Next[0]
#     NextLon = Next[1]
#     endy: int = int(northmost - 100 * NextLat / Scale)
#     endx: int = int(100 * NextLon / Scale - westmost)
#     try:
#         draw.line(((startx, starty), (endx, endy)), fill=(
#             (0, 0, 0)), width=2)
#     except NameError:
#         draw.line(((x, y), (endx, endy)), fill=(
#             (0, 0, 0)), width=2)
#     startx = endx
#     starty = endy

# section: int
# top_range: int
# for ___ in range(1, 8):
#     edge = image.copy()
#     edge = ImageOps.expand(edge, border=2, fill=(220, 220, 220))
#     edge = edge.filter(ImageFilter.Kernel((3, 3), (-1, -1, -1,
#                                                    -1, 8, -1,
#                                                    -1, -1, -1),
#                                           1, 0)).convert('L').point(lambda x: (255 if x > 0 else 0),
#                                                                     mode='1').convert('L'
#                                                                                       ).filter(ImageFilter.BoxBlur(1)).point(lambda x: (255 if x
#                                                                                                                                         > 48 else 0), mode='1').convert('L').crop((2, 2, width + 2,
#                                                                                                                                                                                    height + 2))
#     section = math.floor(32/___)
#     top_range = math.ceil(128/___/___)
#     for (x, y) in itertools.product(range(imgx), range(imgy)):
#         if edge.getpixel((x, y)) == 255 and x not in SeedX and y \
#                 not in SeedY and random.randrange(0, top_range) == 1:
#
#             minweighted: float = 1000000000000
#             j: int
#             minj: int
#             for j in range(num_cells):
#                 weighted = PopulationDensity(Scale * (northmost - y) / 100, Scale * (westmost + x) / 100, SeedLats[j], SeedLons[j]) * OptimizedDistance(
#                     Scale * (northmost - y) / 100, Scale * (westmost + x) / 100, SeedLats[j], SeedLons[j])
#                 if weighted < minweighted:
#                     minweighted = weighted
#                     minj = j
#             if image.getpixel((x, y)) != (nr[minj], ng[minj], nb[minj]):
#                 draw.rectangle([(x - section + 1, y + section - 1), (x + section, y - section)], fill=(
#                     (nr[minj], ng[minj], nb[minj])), outline=((nr[minj], ng[minj], nb[minj])))
#
# edge = image.copy()
# edge = ImageOps.expand(edge, border=2, fill=(220, 220, 220))
# edge = edge.filter(ImageFilter.Kernel((3, 3), (-1, -1, -1,
#                                                -1, 8, -1,
#                                                -1, -1, -1),
#                                       1, 0)).convert('L').point(lambda x: (255 if x > 0 else 0),
#                                                                 mode='1').convert('L'
#                                                                                   ).filter(ImageFilter.BoxBlur(1)).point(lambda x: (255 if x
#                                                                                                                                     > 8 else 0), mode='1').convert('L').crop((2, 2, width + 2,
#                                                                                                                                                                               height + 2))
# for (x, y) in itertools.product(range(imgx), range(imgy)):
#     if edge.getpixel((x, y)) == 255 and x not in SeedX and y \
#             not in SeedY:
#
#         minweighted: float = 1000000000000
#         j: int
#         minj: int
#         for j in range(num_cells):
#             weighted = PopulationDensity(Scale * (northmost - y) / 100, Scale * (westmost + x) / 100, SeedLats[j], SeedLons[j]) * RealDistance(
#                 Scale * (northmost - y) / 100, Scale * (westmost + x) / 100, SeedLats[j], SeedLons[j])
#             if weighted < minweighted:
#                 minweighted = weighted
#                 minj = j
#         if image.getpixel((x, y)) != (nr[minj], ng[minj], nb[minj]):
#             putpixel((x, y), (nr[minj], ng[minj], nb[minj]))

# try:
# for k in range(num_cells):
# draw.rectangle([(int(100*SeedLons[k]/Scale - westmost - rootshortest[j]),int(northmost - 100*SeedLats[k]/Scale - rootshortest(j)),math.ceil(100*SeedLons[k]/Scale - westmost + rootshortest[j]),math.ceil(northmost - 100*SeedLats[k]/Scale + rootshortest(j)))], fill=(nr[j], ng[j], nb[j]), outline=None)
# putpixel((int(100*SeedLons[k]/Scale - westmost), int(northmost - 100*SeedLats[k]/Scale)), (0, 0, 0))
# except Exception:
# pass

image.save('~/Desktop/MVoronoiDiagram.png', 'PNG')
period = time.time() - Ek
image.show()

print(period)
